<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $title; ?> | QUT Surveys</title>
		<meta charset="utf-8">
		<?php echo $meta; ?>

		<style type="text/css">
			<?php echo $styles; ?>
		</style>
		<script type="text/javascript">
			$urlroot = "<?php echo getUrlRoot(); ?>";
		</script>
		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<?php echo $js; ?>
	</head>
	<body>
		<div id="header">
			<img src="<?php echo $urlroot; ?>/images/qut-logo.png" alt="Queensland University of Technology" />
			<div id="loginout">
				<?php if (!$this->isLoggedIn()): ?>
				<a href="<?php echo getUrlToAction('Login'); ?>">Log In</a>
				<?php else: ?>
				<a href="<?php echo getUrlToAction('Login', 'logout'); ?>">Log Out</a>
				<?php endif; ?>
			</div>
			<!--<h1 id="page_title"><?php echo $title; ?></h1>-->
		</div>
		<div id="content">
			<?php echo $content; ?>
		</div>
	</body>
</html>
