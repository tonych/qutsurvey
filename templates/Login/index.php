<?php
if (isset($error) && $error !== NULL && $error != "") {
	$message = "Unknown error occured.";
	if ($error == "noinput")
		$message = "You need to enter in both a username/password.";
	else if ($error == "pwuser")
		$message = "The username/password you have entered was incorrect.";

	$this->loadView("Generic/ErrorBox", [
		'message' => $message,
		]);
}
?>
<div id="loginForm">
	<form action="<?php echo getUrlToAction('Login', 'check'); ?>" method="post">
		<h2>Login</h2>
		<p>
			If you have been given a username and password, you may enter it here to view
			the surveys that are available to you.
		</p>
		<div>
			<label for="username">Username</label>
			<div class="fill"><input type="text" name="username" id="username" /></div>
		</div>
		<div>
			<label for="password">Password</label>
			<div class="fill"><input type="password" name="password" id="password" /></div>
		</div>
		<p>
			<input type="submit" value="Login">
		</p>
	</form>
</div>
