<h2>Login Required</h2>
<p>
	Your session has expired. Click to <a href="<?php echo getUrlToAction('Login'); ?>">log in</a>.
</p>
