<?php
/**
 * KMySqlConnection - MySQL Connection Object
 *
 * This is a MySQL Connection object which uses PDO's MySQL as a backend. It
 * supports prepared statements and caching of previously used statements.
 */
class KMySqlConnection extends KDatabaseConnection {
	/**
	 * Connects to a database server.
	 *
	 * This connects to a database server using the host, username, and
	 * password provided. If one cannot be given, the defaults will be used.
	 *
	 * @param $host
	 *	 The host to connect to. This can be a DNS, IP Address or Socket.
	 * @param $username
	 *	 The username to use when connecting.
	 * @param $password
	 *	 The password to use when connecting.
	 * @param $database
	 *	 The database to connect to.
	 *
	 * @return
	 *	 Returns TRUE on success, FALSE on failure.
	 */
	public function connect($host, $username, $password, $database, $tag = NULL) {
		parent::connect($host, $username, $password, $database, $tag);

		if ($this->connected) {
			$this->close();
		}

		try {
			$this->handle = new PDO(
				sprintf('mysql:host=%s;dbname=%s;', $host, $database),
				$username,
				$password,
				array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
				);
			$this->handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (PDOException $e) {
			throw new KDatabaseException(t('Could not connect to database'));
		}

		return TRUE;
	}

	/**
	 * Selects data from a table.
	 *
	 * This selects data from a table. It is equivilent to a SELECT .. FROM
	 * query. It will automatically escape arguments given.
	 *
	 * @param $table
	 *	 The table you wish to select data from.
	 * @param $where
	 *	 An associative array containing the data which you would like to
	 *	 match.
	 * @param $fields
	 *	 An array of fields, stating which specific fields you would like to
	 *	 retrieve.
	 *
	 * @return DatabaseResultInterface
	 *	 Returns a DatabaseResultInterface containing the results.
	 */
	public function select($table, $where = array(), $fields = null, $sort = null, $sortOrder = 'asc', $limit = -1, $offset = 0) {
		// Create Fields
		$f = '';
		$num_fields = count($fields);
		if (is_array($fields) && $num_fields > 0) {
			for($i = 0; $i < $num_fields; $i++) {
				$f .= $fields[$i];
				if (($i + 1) < $num_fields) {
					$f .= ", ";
				}
			}
		}
		else {
			$f = '*';
		}

		// Create WHERE area.
		$w = '';
		$fields = array_keys($where);
		$num_where = count($fields);
		for ($i = 0; $i < $num_where; $i++) {
			if (is_null($where[$fields[$i]])) {
				$w .= sprintf(
					"%s IS NULL",
					$fields[$i]
					);
			}
			else {
				$w .= sprintf(
					"%s = ?",
					$fields[$i]
					);
			}

			if (($i+1) < $num_where) {
				$w .= ' AND ';
			}
		}

		// Create ORDER BY
		$s = '';
		if ($sort !== NULL) {
			if (strtolower($sortOrder) != 'asc' && strtolower($sortOrder) != 'desc') {
				$sortOrder = 'asc';
			}
			$s = sprintf('ORDER BY %s %s', $sort, strtoupper($sortOrder));
		}

		// Create LIMITS
		$l = '';
		if ($limit > 0 && $offset >= 0) {
			$l = sprintf('LIMIT %d, %d', $offset, $limit);
		}

		// Create Query and execute.
		if ($num_where > 0) {
			$query = "SELECT {$f} FROM {$table} WHERE {$w} {$s} {$l}";
		}
		else {
			$query = "SELECT {$f} FROM {$table} {$s} {$l}";
		}

		try {
			$q = $this->getPreparedQuery($query);
		}
		catch (KDatabaseException $e) {
			throw $e;
		}
		$this->incrementQueryCount($query);

		// Bind Params.
		for ($i = 0; $i < $num_where; $i++) {
			if (is_null($where[$fields[$i]])) {
				continue;
			}

			switch (true) {
				case is_bool($where[$fields[$i]]):
					$type = PDO::PARAM_BOOL;
					break;
				case is_int($where[$fields[$i]]):
					$type = PDO::PARAM_INT;
					break;
				default:
					$type = PDO::PARAM_STR;
					break;
			}
			$q->bindValue($i+1, $where[$fields[$i]], $type);
		}

		$success = $q->execute();

		return new KDatabaseResult($q, $success);
	}
}
