<?php
//-----------------------------------------------------------------------------
// KDB Package for Kakera 2.x
//	 This package contains an interface to access databases.
//-----------------------------------------------------------------------------

/*---------------------------------------------------------------------------//
Changelog:
	2.0:
		- Initial Release of KDB.
//---------------------------------------------------------------------------*/

//-----------------------------------------------------------------------------
// General Metadata
//-----------------------------------------------------------------------------
$module['author']	= "Tony Chau";
$module['version']	= "2.1";
$module['name']		= "KDB Package";
$module['description'] = 
	"This package contains an interface to access databases.";

//-----------------------------------------------------------------------------
// Dependencies
//-----------------------------------------------------------------------------
$module['dependencies'] = array();

//-----------------------------------------------------------------------------
// Normal Files which need to be included once a class has been loaded.
//-----------------------------------------------------------------------------
$module['files'] = array('KDBExtension.php');

//-----------------------------------------------------------------------------
// Autoload Table
//-----------------------------------------------------------------------------
$module['autoload'] = array(
	'KMySqlConnection'	  => 'mysql/KMySqlConnection.php',
	'KDatabaseResult'	  => 'KDatabaseResult.php',
	'KDatabaseConnection' => 'KDatabaseConnection.php',
	'KDatabaseException'  => 'KDatabaseException.php',
	);

//------------------------------------------------------------------------------
// Misc.
//------------------------------------------------------------------------------
$module['load_immediately'] = TRUE;

return $module;
