<?php
class KDBExtension extends Extension {
	protected $dbConnections = array();
	protected $defaultDbId = NULL;
	
	public function hasMethods() {
		return array('getDatabaseConnection');
	}
	
	public function getPackage() {
		return "KDB";
	}
	
	public function getDatabaseConnection($dbname = NULL) {
		if ($dbname == NULL) {
			$dbname = $this->defaultDbId;
		}
		
		if (isset($this->dbConnections[$dbname]))
			return $this->dbConnections[$dbname];
		
		throw new KDatabaseException("Non-existant Database Connection specified.");
	}
	
	public function createDatabaseLinks($dbOpts) {
		if (count($dbOpts) >= 1) {
			foreach ($dbOpts as $dbId => $opts) {
				if (isset($opts['dbClass'])
				&& isset($opts['dbHost'])
				&& isset($opts['dbUser'])
				&& isset($opts['dbPass'])
				&& isset($opts['dbName']))
				{
					$this->dbConnections[$dbId] = new $opts['dbClass']();
					$this->dbConnections[$dbId]->connect(
						$opts['dbHost'],
						$opts['dbUser'],
						$opts['dbPass'],
						$opts['dbName'],
						$dbId
						);
		
					if (isset($opts['default']) && $opts['default']) {
						$this->defaultDbId = $dbId;
					}
				}
			}
		
			if (count($dbOpts) == 1) {
				$keys = array_keys($this->dbConnections);
				$this->defaultDbId = array_pop($keys);
			}
			elseif ($this->defaultDbId === NULL) {
				throw new KDatabaseException(
					t('Multiple Database connections established, but no default '
					.'connection set.')
					);
			}
		}
	}
}

// Register Extension.
$extension = new KDBExtension();
$dbOpts = kernel()->config()->get('app/db', array());
$extension->createDatabaseLinks($dbOpts);
kernel()->attachExtension($extension);