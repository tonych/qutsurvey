<?php
// Deprecated Function
function get_random_bytes($numbytes, $hex = FALSE) {
	return getRandomBytes($numbytes, $hex);
}

function getRandomBytes($numbytes, $hex = FALSE) {
	if ($numbytes <= 0)
		return FALSE;
	
	if (function_exists('openssl_random_pseudo_bytes')) {
		$string = openssl_random_pseudo_bytes($numbytes, $strong);
	}
	else {
		$string = "";
		for ($i = 0; $i < $numbytes; $i++) {
			$string .= chr(mt_rand(0, 255));
		}
	}
	
	if ($hex)
		return bin2hex($string);
	
	return $string;
}