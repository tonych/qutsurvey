<?php
/**
 * KAccessDeniedException - Access Denied Exception
 *
 * This occurs when the user attempts to do something which they do not have
 * privileges to do.
 */
class KAccessDeniedException extends KException {}