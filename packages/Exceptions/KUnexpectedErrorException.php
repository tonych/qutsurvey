<?php
/**
 * KUnexpectedErrorException - Unexpected Error Exception
 *
 * This occurs when even after an operation which should have resulted in a 
 * specific result, does not.
 */
class KUnexpectedErrorException extends KException {}