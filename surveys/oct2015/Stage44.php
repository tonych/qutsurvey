<?php
class Stage44Controller extends StageController {
	public function start() {
		parent::start();
	}

	public function doTask() {
		global $config;

		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete", ['survey' => $this->survey['survey_id']]);
		}

		// Get number of matrices correct in Task4-1
		$sql = "SELECT * FROM surveys_log WHERE user_id = ? AND survey_id = ? AND event LIKE 'Task 4-1 Matrix % Complete.'";
		$stmt = $this->dbc->getPreparedQuery($sql);
		$stmt->bindValue(1, $this->getCurrentUser()['user_id'], PDO::PARAM_INT);
		$stmt->bindValue(2, $this->survey['survey_id'], PDO::PARAM_INT);
		$success = $stmt->execute();
		$rs = new KDatabaseResult($stmt, $success);
		$events = $rs->fetchAll();

		$numCorrect = 0;
		foreach ($events as $ev) {
			$d = json_decode($ev['data'], true);
			if ($d['isCorrect'])
				$numCorrect++;
		}

		// Pass Variables to View.
		$this->viewBag["numCorrect"] = $numCorrect;
		// END.
	}

	public function doTaskCheck() {
		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete");
		}

		// Check Variables
		if (!array_key_exists('paymentMethod', $_POST) || !isset($_POST['paymentMethod']) || $_POST['paymentMethod'] * 1 != $_POST['paymentMethod'])
		{
			$this->redirectToStage($this->stage, "doTask");
		}

		// Save
		$this->surveyController->saveEvent("task44-payment", $_POST['paymentMethod']);

		// Move to next stage.
		$this->redirectToStage($this->stage, "nextStage");
	}
}
