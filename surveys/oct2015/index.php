<?php
// load survey config
$survey_dir = dirname(__FILE__);
$survey_package = include_once "{$survey_dir}/info.php";

// Get which stage we're up to.
$stage = $this->loadEvent("currStage");
if (is_null($stage)) {
	// Create their current stage
	$stage = $survey_package['stages'][0];
	$this->saveEvent("currStage", $stage);
	$this->saveEvent("Started Survey");
}
else {
	$stage = $stage['data'];
	if (!in_array($stage, $survey_package['stages'])) {
		$this->redirect("Errors", "stageMissing");
	}
}

// Load stage.
$stagePath = SURVEY_PATH . $relPath . "stages/{$stage}.php";
if (checkPath($stagePath, SURVEY_PATH)) {
	include_once $stagePath;
}
