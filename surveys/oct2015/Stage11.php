<?php
class Stage11Controller extends StageController {
	public function start() {
		parent::start();

		
		// Nothing to do here, show the introduction page.
	}

	public function doTask() {
		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete", ['survey' => $this->survey['survey_id']]);
		}

		// Otherwise load up the task.
		// END.
	}

	public function doTaskCheck() {
		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete");
		}

		// Do checks.
		$opts = [];
		for ($i = 0; $i < 20; $i++) {
			$k = "opt_" . ($i + 1);
			if (array_key_exists($k, $_POST) && !is_null($_POST[$k]) && ($_POST[$k] == 'a' || $_POST[$k] == 'b')) {
				$opts[$i] = $_POST[$k];
			}
		}

		// Check they have chosen all the options.
		if (count($opts) != 20) {
			$this->redirectToStage($this->stage, 'doTask');
		}

		// Save
		$this->surveyController->saveEvent("task1-1-data", $opts);

		// Move to next stage.
		$this->redirectToStage($this->stage, "nextStage");
	}
}
