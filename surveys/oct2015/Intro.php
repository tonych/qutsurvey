<?php
class IntroController extends StageController {
	public function start() {
		parent::start();

		global $config;
		$dateFormat = $config->get("system/timeFormat", "Y/m/d, h:ia");

		$this->viewBag['start_date']  = $this->survey['start_date'];
		$this->viewBag['end_date']    = $this->survey['end_date'];
		$this->viewBag['date_format'] = $dateFormat;
	}
}
