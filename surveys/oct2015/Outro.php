<?php
class OutroController extends StageController {
	public function start() {
		parent::start();

		// Set Survey Complete Flag.
		$this->dbc->update('user_surveys',
			['complete' => 1],
			['survey_id' => $this->survey['survey_id'], 'user_id' => $this->getCurrentUser()['user_id']]
		);
	}
}
