<h2>Task 4 Part 5</h2>
<p>
    Guess what rank you achieved in Part 1 and Part 2 of this test where 1 is the highest and 1000 being the lowest.
</p>
<form id="taskForm" method="post" action="<?php echo $this->getUrlToStage($this->stage, 'doTaskCheck'); ?>">
    <p id="error">
        Enter a number between 1 and 1000.
    </p>
    <p>
        <label for="part1txt">Part 1</label> <input type="text" name="part1txt" id="part1txt">
    </p>
    <p>
        <label for="part2txt">Part 2</label> <input type="text" name="part2txt" id="part2txt">
    </p>
    <input type="submit" id="submitBtn" value="Next">
</form>
<script type="text/javascript">
    function validInput(n) {
        return !(n != n * 1  || n <= 0 || n > 1000);
    }

    $(function() {
        $("#taskForm").submit(function() {
            var v1 = $("#part1txt").val();
            var v2 = $("#part2txt").val();
            var valid = true;

            if (!(valid = valid && validInput(v1))) {
                $("#part1txt").css('background-color', '#ff9999');
            }
            if (!(valid = valid && validInput(v2))) {
                $("#part2txt").css('background-color', '#ff9999');
            }

            if (!valid)
                $("#error").show();

            return valid;
        });
    });
</script>
