	<h2>Task 3 - Balloon <?php echo $currBalloon; ?></h2>
	<form id="taskForm" method="post" action="<?php echo $this->getUrlToStage($this->stage, 'doTaskCheck'); ?>">
		<input type="hidden" id="e" name="e" value="0.0">
		<input type="hidden" id="p" name="p" value="0">
		<input type="hidden" id="m" name="m" value="0">
		<input type="hidden" id="b" name="b" value="<?php echo $currBalloon; ?>">
		<div class="fullWidth">
			<p>Earnings: $<span id="earnings">0.00</span>
			<p id="task3buttons">
				<input type="button" id="pump" value="Pump up the balloon">
				<input type="submit" value="Collect your earnings">
			</p>
			<p><img src="<?php echo getUrlRoot('images/ball.jpg'); ?>" alt="Balloon" id="balloon" /></p>
		</div>
	</form>
	<script type="text/javascript">
		var p,e,m;
		$(function() {
			$("#pump").click(function() {
				// They've already blown up their balloon.
				if (p >= m) {
					document.getElementById("pump").disabled = true;
					return;
				}

				p++;
				if (p < m) {
					e += 0.5;
					w += 10;
					h += 10;
					updateBalloonSize(w, h);
				}
				else {
					document.getElementById("pump").disabled = true;
					document.getElementById("pump").value = "Balloon Popped";
					document.getElementById("balloon").style.display = "none";
					e = 0;
				}

				document.getElementById("p").value = p;
				document.getElementById("e").value = e;
				document.getElementById("earnings").innerHTML = e.toFixed(2);
			});

			w = 25;
			h = 25;
			p = 0;
			e = 0;
			m = Math.floor(Math.random() * 128 + 1);
			document.getElementById("m").value = m;
			updateBalloonSize(w, h);
		});

		function updateBalloonSize(width, height) {
			document.getElementById("balloon").style.height = height + 'px';
			document.getElementById("balloon").style.width = width + 'px';
		}
	</script>
