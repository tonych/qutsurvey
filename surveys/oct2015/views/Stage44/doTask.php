	<h2>Task 4 Part 4</h2>
	<p>
		Select which payment method you would like to get paid by.
	</p>
	<ol>
		<li><strong>Method 1</strong>: $1 for each correct answer</li>
		<li><strong>Method 2</strong>: $3 for each correct answer if you ranked in the top 3rd of all participants.</li>
	</ol>
	<p>You got <?php echo $numCorrect; ?> correct in Task 4 Part 1.</p>
	<form id="taskForm" method="post" action="<?php echo $this->getUrlToStage($this->stage, 'doTaskCheck'); ?>">
			<input type="submit" id="submitBtn" name="paymentMethod" value="Method 1">
			<input type="submit" id="submitBtn" name="paymentMethod" value="Method 2">
	</form>
