	<h2>Task 4 Part 1 - Matrix <?php echo $currMatrix; ?></h2>
	<p>
		Select 2 numbers which add up to 10.
	</p>
	<p>Time remaining: <span id="countdown"></span></p>
	<form id="taskForm" method="post" action="<?php echo $this->getUrlToStage($this->stage, 'doTaskCheck'); ?>">
		<input type="hidden" id="x" name="x" value="-1">
		<input type="hidden" id="y" name="y" value="-1">
		<input type="hidden" id="m" name="m" value="<?php echo $currMatrix; ?>">
		<div class="fullWidth">
			<table id="matrix">
				<tr>
				<?php
				for ($i = 0; $i < count($matrix); $i++):
					if ($i % 4 == 0 && $i != 0):
						echo "</tr><tr>";
					endif;
					echo "<td data-mid='{$i}'>". sprintf("%.1f", $matrix[$i] / 10) . "</td>";
				endfor;
				?>
				</tr>
			</table>
			<input type="submit" id="submitBtn" value="Next" disabled="disabled">
		</div>
	</form>
	<script type="text/javascript">
		var selected = [];
		var expiry = "<?php echo $expiryTime; ?>";

		$(function () {
			$("#matrix td").click(function() {
				var mid = $(this).data('mid');
				if (selected.length >= 2)
					selected.shift();
				selected.push(mid);
				if (selected.length >= 2 && selected[0] == selected[1])
					selected.pop();

				// remember data.
				if (selected.length >= 2) {
					$("#y").val(selected[1]);
					$("#x").val(selected[0]);
				}
				if (selected.length == 1) {
					$("#x").val(selected[0]);
					$("#y").val(-1);
				}

				// Highlight the selected boxes
				var boxes = $("#matrix td");
				$(boxes).removeClass('highlight');
				for (var i = 0; i < selected.length; i++) {
					$(boxes[selected[i]]).addClass('highlight');
				}

				// allow submit
				if (selected.length == 2) {
					document.getElementById("submitBtn").disabled = false;
				}
				else {
					document.getElementById("submitBtn").disabled = true;
				}
			});

			// Highlight the selected boxes
			var boxes = $("#matrix td");
			$(boxes).removeClass('highlight');
			for (var i = 0; i < selected.length; i++) {
				$(boxes[selected[i]]).addClass('highlight');
			}

			// Compatibility for older browsers
			if (!Date.now) {
			    Date.now = function() { return new Date().getTime(); }
			}

			// Start the timer
			setTimeout(timer, 500);
		});

		function timer() {
			var currTime = Date.now() / 1000;
			var timeLeft = expiry - currTime;

			if (timeLeft < 0) {
				window.location = "<?php echo $this->getUrlToStage($this->stage, 'complete'); ?>";
			}

			var mins = Math.floor(timeLeft / 60);
			var secs = Math.abs(Math.floor(timeLeft % 60));

			var time = mins + ":";
			if (secs.toString().length == 1)
				time += "0";
			time += secs;

			$("#countdown").html(time);
			setTimeout(timer, 500);
		}
	</script>
