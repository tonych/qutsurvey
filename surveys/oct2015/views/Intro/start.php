<h2>Welcome to this Survey</h2>
<p>
	Welcome to this survey. This survey will take about 45 minutes of your time,
	please make sure you set aside enough time to complete the following tasks.
	This survey has been...
</p>
<p>
	Please update this message as you deem fit, including such items as:
</p>
<ul>
	<li>Time required</li>
	<li>Materials required</li>
	<li>Ethics Message</li>
	<li>Privacy</li>
	<li>Messages of how they'll be contacted in future/rewards</li>
	<li>Once you have completed a task, you cannot move back.</li>
</ul>
<p>
	This survey is available between:
		<?php echo date($date_format, $start_date); ?> and
		<?php echo date($date_format, $end_date); ?>.
</p>
<p id="surveyNav">
	<a href="<?php echo $this->getUrlToStage($this->stage, 'nextStage'); ?>">
		Next
	</a>
</p>
