	<h2>Task 1</h2>
	<ul id="taskPart">
		<li>1</li>
		<li class="selected">2</li>
	</ul>
	<form id="taskForm" method="post" action="<?php echo $this->getUrlToStage($this->stage, 'doTaskCheck'); ?>">
		<div class="fullWidth">
			<div class="floatLeft halfWidth">
					<?php for ($i = 1; $i <= 20; $i++): ?>
						<p class="smallerFont noMargin">
							<?php
							$opt = "opt_{$i}";
								echo '
									<label for="'. $opt .'a">Draw from bag <input type="radio" class="radioA" name="'. $opt .'" value="a" id="'. $opt .'a"></label>
									or
									<label for="'. $opt .'b"><input type="radio" class="radioB" name="'. $opt .'" value="b" id="'. $opt .'b"> $'. $i .' for sure</label>
									';
							?>
						</p>
					<?php endfor; ?>
			</div>
			<div class="floatRight halfWidth">
				<!-- TODO: Insert right-side explanation to match the video -->
			</div>
		</div>
		<p id="surveyNav" class="floatClear">
			<input type="submit" value="Next">
		</p>
	</form>
	<script type="text/javascript">
		$(function() {
			$("#taskForm input[type=radio]").click(function() {
				var re = /opt_(\d+)(\w)/g;
				var res = re.exec($(this).attr('id'));
				if (res[2] == "a") {
					// Select all radio buttons on A side including this one.
					// then Bs.
					var doUntil = res[1] * 1;
					var radioBtn = $("#taskForm input.radioA");
					for (var i = 1; i <= 20; i++) {
						if (i <= doUntil) {
							var n = "#opt_" + i + "a";
							$(n).prop('checked', true);
						}
						else {
							var n = "#opt_" + i + "b";
							$(n).prop('checked', true);
						}
					}
				}
				else if (res[2] == "b") {
					// Select all radio buttons on A side up to this one, then
					// Bs including this one.
					var doUntil = res[1] * 1;
					var radioBtn = $("#taskForm input.radioA");
					for (var i = 1; i <= 20; i++) {
						if (i < doUntil) {
							var n = "#opt_" + i + "a";
							$(n).prop('checked', true);
						}
						else {
							var n = "#opt_" + i + "b";
							$(n).prop('checked', true);
						}
					}
				}
			});
		});
	</script>
