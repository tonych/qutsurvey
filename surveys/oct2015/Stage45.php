<?php
class Stage45Controller extends StageController {
	public function start() {
		parent::start();
	}

	public function doTask() {
		global $config;

		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete", ['survey' => $this->survey['survey_id']]);
		}

		// END.
	}

	public function doTaskCheck() {
		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete");
		}

		// Check Variables
		if (!array_key_exists('part1txt', $_POST) || !isset($_POST['part1txt']) || $_POST['part1txt'] * 1 != $_POST['part1txt'] ||
			!array_key_exists('part2txt', $_POST) || !isset($_POST['part2txt']) || $_POST['part2txt'] * 1 != $_POST['part2txt'])
		{
			$this->redirectToStage($this->stage, "doTask");
		}

		// Save
		$this->surveyController->saveEvent("task45-rank-part1", $_POST['part1txt'] * 1);
		$this->surveyController->saveEvent("task45-rank-part2", $_POST['part2txt'] * 1);

		// Move to next stage.
		$this->redirectToStage($this->stage, "nextStage");
	}
}
