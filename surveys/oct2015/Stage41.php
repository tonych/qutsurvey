<?php
class Stage41Controller extends StageController {
	public function start() {
		parent::start();

		if ($this->surveyController->loadEvent("task41-currMatrix") === NULL) {
			$this->surveyController->saveEvent("task41-currMatrix", 1);
		}
	}

	public function complete() {
		// Do Nothing really.
	}

	public function doTask() {
		global $config;

		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete", ['survey' => $this->survey['survey_id']]);
		}

		// Otherwise load up the task.
		$ev = $this->surveyController->loadEvent("task41-currMatrix");
		$matrixN = $ev['data'];

		// Get start time.
		$ev = $this->surveyController->loadEvent("task41-startTime");
		$startTime = $ev['data'];
		if ($startTime === NULL) {
			$startTime = time();
			$this->surveyController->saveEvent("task41-startTime", $startTime);
		}
		$expiryTime = $startTime + $this->info["task4TimeLimit"];

		// Generate Matrix
		$matrix = task4GenerateMatrix($this->info['randomSeed'] + $this->info['seedIncrement'] * $matrixN - 1);

		// Pass Variables to View.
		$this->viewBag["currMatrix"] = $matrixN;
		$this->viewBag["matrix"]     = $matrix;
		$this->viewBag["startTime"]  = $startTime;
		$this->viewBag["expiryTime"]  = $expiryTime;
		// END.
	}

	public function doTaskCheck() {
		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete");
		}

		// Check Variables
		if (!array_key_exists('m', $_POST) || !array_key_exists('x', $_POST) || !array_key_exists('y', $_POST) ||
			!isset($_POST['m']) || !isset($_POST['x']) || !isset($_POST['y']) ||
			$_POST['m'] * 1 != $_POST['m']  || $_POST['x'] * 1 != $_POST['x']  || $_POST['y'] * 1 != $_POST['y'])
		{
			$this->redirectToStage($this->stage, "doTask");
		}

		// Check current time
		$currTime = time();
		$ev = $this->surveyController->loadEvent("task41-startTime");
		$startTime = $ev['data'];
		$expiryTime = $startTime + $this->info["task4TimeLimit"];

		if ($currTime > $expiryTime) {
			$this->redirectToStage($this->stage, "complete");
		}

		//
		$matrixN = $_POST['m'] * 1;
		$s = [$_POST['x'] * 1, $_POST['y'] * 1];
		$matrix = task4GenerateMatrix($this->info['randomSeed'] + $this->info['seedIncrement'] * ($matrixN - 1));
		$correct = $matrix[$s[0]] + $matrix[$s[1]] == 100;

		// Save data
		$this->surveyController->saveEvent("Task 4-1 Matrix {$matrixN} Complete.", ["isCorrect" => $correct]);
		$this->surveyController->saveEvent("task41-currMatrix", ++$matrixN);

		// Move to next stage.
		if ($currTime > $expiryTime) {
			$this->redirectToStage($this->stage, "complete");
		}
		else {
			$this->redirectToStage($this->stage, "doTask");
		}
	}
}
