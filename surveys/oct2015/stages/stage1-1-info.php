<?php
	global $config;
	$dateFormat = $config->get("system/timeFormat", "Y/m/d, h:ia");

	$this->setTitle($survey['survey_name']);
?>
<?php if (SURVEY_MODE == "resume"): ?>
	<h2>Task 1</h2>
	<p>
		This task will have 2 parts.
	</p>
	<div style="border: 1px solid #eee; width: 500px; height: 500px; margin: 10px auto; text-align: center;">
		Video will be placed here.
	</div>
	<p id="surveyNav">
		<a href="<?php echo getUrlToAction('Survey', 'check', ['survey' => $survey['survey_id']]); ?>">
			Next
		</a>
	</p>
<?php elseif (SURVEY_MODE == "check"): ?>
	<?php
	// Advance to next stage.
	for ($i = 0; $i < count($survey_package['stages']); $i++) {
		if ($survey_package['stages'][$i] == $stage) {
			$stage = $survey_package['stages'][$i + 1];
			$this->saveEvent("Task 1 Introduction Complete.");
			$this->saveEvent("currStage", $stage);
			break;
		}
	}

	$this->redirect('Survey', 'resume', ['survey' => $survey['survey_id']]);
	?>
<?php else:
	$this->redirect("Errors", "stageModeNotImplemented");
endif;
