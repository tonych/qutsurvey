<?php
	global $config;
	$dateFormat = $config->get("system/timeFormat", "Y/m/d, h:ia");

	$this->setTitle($survey['survey_name']);
?>
<?php if (SURVEY_MODE == "resume"): ?>
	<h2>Welcome to this Survey</h2>
	<p>
		Welcome to this survey. This survey will take about 45 minutes of your time,
		please make sure you set aside enough time to complete the following tasks.
		This survey has been...
	</p>
	<p>
		Please update this message as you deem fit, including such items as:
	</p>
	<ul>
		<li>Time required</li>
		<li>Materials required</li>
		<li>Ethics Message</li>
		<li>Privacy</li>
		<li>Messages of how they'll be contacted in future/rewards</li>
		<li>Once you have completed a task, you cannot move back.</li>
	</ul>
	<p>
		This survey is available between:
			<?php echo date($dateFormat, $survey['start_date']); ?> and
			<?php echo date($dateFormat, $survey['end_date']); ?>.
	</p>
	<p id="surveyNav">
		<a href="<?php echo getUrlToAction('Survey', 'check', ['survey' => $survey['survey_id']]); ?>">
			Next
		</a>
	</p>
<?php elseif (SURVEY_MODE == "check"): ?>
	<?php
	// Advance to next stage.
	for ($i = 0; $i < count($survey_package['stages']); $i++) {
		if ($survey_package['stages'][$i] == $stage) {
			$stage = $survey_package['stages'][$i + 1];
			$this->saveEvent("Introduction Complete.");
			$this->saveEvent("currStage", $stage);
			break;
		}
	}

	$this->redirect('Survey', 'resume', ['survey' => $survey['survey_id']]);
	?>
<?php else:
	$this->redirect("Errors", "stageModeNotImplemented");
endif;
