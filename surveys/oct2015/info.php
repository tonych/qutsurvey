<?php
if (!function_exists("task4GenerateMatrix")) {
	function task4GenerateMatrix($seed, $len = 12) {
		// Randomly Generate Matrix using Initial Seed + Increment * Matrix Number
		// This ensures that the matrix is always predictable and can always be recomputed.
		mt_srand($seed);

		// Generate locations for our "matching pair"
		$x = mt_rand(0, $len - 1);
		do {
			$y = mt_rand(0, $len - 1);
		} while ($x == $y);

		// Generate values of our "matching pair"
		$xN = mt_rand(1,99);
		$yN = 100 - $xN;

		// Generate the matrix, and place our matching pair in there.
		$matrix = [];
		for ($i = 0; $i < $len; $i++)
			$matrix[$i] = -1;
		$matrix[$x] = $xN;
		$matrix[$y] = $yN;

		// Generate random numbers ensuring that they do not exist in the matrix already.
		for ($i = 0; $i < count($matrix); $i++) {
			if ($i == $x || $i == $y)
				continue;

			$n = mt_rand(1,99);
			$m = 100 - $n;
			$exists = FALSE;
			for ($j = 0; $j < count($matrix); $j++) {
				if ($n == $matrix[$j] || $m == $matrix[$j]) {
					$exists = TRUE;
					break;
				}
			}

			if ($exists)
				$i--;
			else
				$matrix[$i] = $n;
		}

		return $matrix;
	}
}

return [
	"name" => [
		
	],
	"admin" => [
		["Participants", "Participants"],
		["Results", "Results"],
	],
	"stages" => [
		["Intro", "Introduction"],
		["Stage11", "Task 1 Part 1"],			// pull ball out of the bag, can see ball.
		["Stage12", "Task 1 Part 2"],			// pull ball out of the bag, can't see ball.
		["Stage21", "Task 2 Part 1"],			// No information yet.
		["Stage31", "Task 3 Part 1"],			// blow up the balloon
		["Stage41", "Task 4 Part 1"],			// matrix task
		["Stage42", "Task 4 Part 2"],			// matrix task, different payment
		["Stage43", "Task 4 Part 3"],			// matrix task, can choose payment style
		["Stage44", "Task 4 Part 4"],			// given correct answers, choose payment style
		["Stage45", "Task 4 Part 5"],			// guess your rank
		["Outro", "Conclusion"],
	],
	"viewPath"    => dirname(__FILE__) . '/views/',
	"defaultPage" => "start",

	// Random Seed in which the random number generator will be based off to generate
	// the matrices.
	//
	// Based off the Mersenne Twister random number generator.
	//   https://en.wikipedia.org/wiki/Mersenne_Twister
	"randomSeed"     => 47385912,

	// Number to increase the seed each time by when the matrix is created each time.
	"seedIncrement"  => 1581,

	// Time limit of the matrix task in seconds.
	"task4TimeLimit" => 60 * 3,
];
