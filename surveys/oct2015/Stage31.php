<?php
class Stage31Controller extends StageController {
	public function start() {
		parent::start();

		if ($this->surveyController->loadEvent("task3-currBalloon") === NULL) {
			$this->surveyController->saveEvent("task3-currBalloon", 1);
		}
	}

	public function doTask() {
		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete", ['survey' => $this->survey['survey_id']]);
		}

		// Otherwise load up the task.
		$ev = $this->surveyController->loadEvent("task3-currBalloon");
		$this->viewBag["currBalloon"] = $ev['data'];
		// END.
	}

	public function doTaskCheck() {
		// Check that they have not already completed this task.
		if ($this->isStageComplete()) {
			$this->redirect("Errors", "stageComplete");
		}

		// Do checks.
		if (!array_key_exists('p', $_POST) || !array_key_exists('e', $_POST) || !array_key_exists('m', $_POST) || !array_key_exists('b', $_POST) ||
			is_null($_POST['p']) || is_null($_POST['e']) || is_null($_POST['m']) || is_null($_POST['b']))
		{
			$this->redirectToStage($this->stage, "doTask");
		}

		// Convert to numbers/int.
		$p = $_POST['p'] * 1;
		$e = $_POST['e'] * 1;
		$m = $_POST['m'] * 1;
		$b = $_POST['b'] * 1;

		// Get previously saved state
		$ev = $this->surveyController->loadEvent("task3-currBalloon");
		$currBalloon = $ev['data'];

		// Prevent overwriting data or adding duplicate data by pressing Back and resubmitting the form.
		if ($b < $currBalloon) {
			$this->redirectToStage($this->stage, "doTask");
		}

		// Get currently saved data
		$ev = $this->surveyController->loadEvent("task3-data");
		$data = $ev['data'];
		if ($data === NULL)
			$data = [];

		// Add to data
		$data[] = [
			'pumps'    => $p,
			'earnings' => $e,
			'max'      => $m,
		];

		// Save
		$this->surveyController->saveEvent("task3-data", $data);
		$this->surveyController->saveEvent("task3-currBalloon", ++$currBalloon);

		// Move to next stage.
		if ($currBalloon > 5)
			$this->redirectToStage($this->stage, "nextStage");
		else
			$this->redirectToStage($this->stage, "doTask");
	}
}
