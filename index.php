<?php
// Include the configuration file. This is required!
if (!file_exists("config.php"))
	die("Could not find configuration.");

$config = include_once "config.php";

// Define constants for our paths.
define("BASE_PATH", "{$config['system/paths']['BASE']}/");
define("CORE_PATH", BASE_PATH . "{$config['system/paths']['CORE']}");
define("TEMPLATES_PATH", BASE_PATH . "{$config['system/paths']['TEMPLATES']}");
define("PAGES_PATH", BASE_PATH . "{$config['system/paths']['PAGES']}");
define("TASKS_PATH", BASE_PATH . "{$config['system/paths']['TASKS']}");
define("SURVEY_PATH", BASE_PATH . "{$config['system/paths']['SURVEYS']}");

// Magic Quote Cleanup.
// Due to using Closures, this requires PHP 5.3.0+. As Magic Quotes no
// longer exist PHP 6, this will be strictly PHP <5.3 and >6
if((function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) ||
		(ini_get('magic_quotes_sybase') && (strtolower(ini_get('magic_quotes_sybase')) != "off")))
{
	$array_unquote = function (&$data, $key) {
			$data = stripslashes($data);
		};

	array_walk_recursive($_GET, $array_unquote);
	array_walk_recursive($_POST, $array_unquote);
	array_walk_recursive($_COOKIE, $array_unquote);
}

// Clean Global Space.
$keys = array_keys($GLOBALS);
$allowed = array('GLOBALS', '_POST', '_GET', '_REQUEST', '_ENV', '_SERVER', '_COOKIE', '_FILES', 'config');

foreach($keys as $k) {
	if (!in_array($k, $allowed)) {
		unset($GLOBALS[$k]);
	}
}

// Set MBString Encoding.
mb_internal_encoding("UTF-8");

// Set DateTime
date_default_timezone_set($config["system/timezone"]);

// Autoload
spl_autoload_register(function($class) {
	if (file_exists(CORE_PATH . "/{$class}.php")) {
		include_once CORE_PATH . "/{$class}.php";
	}
});

// Include all Core PHP files needed.
$core_f = scandir(CORE_PATH);
foreach ($core_f as $f) {
	if (pathinfo(CORE_PATH . "/{$f}", PATHINFO_EXTENSION) == "php") {
		include_once CORE_PATH . "/{$f}";
	}
}

// Start Kakera Compatibility Shim
$config_obj = new Config();
$config_obj->add($config);
$config = $config_obj;
unset($config_obj);
$kernel = new Kernel();
$kernel->packages()->addPackagePath(BASE_PATH . 'packages/');

// Disable caching
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

// Find route that matches the current path.
if (!isset($_GET['page'])) {
	$page = $config['app/defaultpage'];
}
else {
	$page = trim($_GET['page']);
}

// Check if it exists
if (checkPath(PAGES_PATH . "/$page.php", PAGES_PATH)) {
	// Include it
	include_once PAGES_PATH . "/$page.php";

	// Get action
	if (!isset($_GET['action'])) {
		$action = "index";
	}
	else {
		$action = $_GET['action'];
	}

	// Load Controller class
	$controllerName = ucfirst($page) . "Controller";
	$controller = new $controllerName();
	$controller->loadAction($action);
}
else {
	echo f($page) . " not found.";
}
