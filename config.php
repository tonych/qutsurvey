<?php
/**
 * Configuration settings go in here.
 *   The comma after the last element in an array is placed there on purpose. It's legal PHP Syntax and
 *   recommended in the many PHP coding standards.
 *
 *   Due to the use of the shorthand array syntax, at least PHP 5.4.0 is required.
 */
return [
	/**
	* KDB Database Connection Options
	*
	* This is where you can setup your database support. You can opt to not have
	* a database connection in which you can just leave this as a blank array
	* (i.e. array()).
	*
	* If you choose to have a database connection, you can assign ID's for
	* multiple database connections which can be accessed by doing
	* getDbConnection($id).
	*
	* If you do have multiple database connections, one must be assigned the
	* default database connection.
	*
	* Note that if multiple are set as "default", only the last one will take
	* effect.
	*/
	'app/db' => array(
		'defaultDb' => array(
			'dbClass' => 'KMySqlConnection',
			'dbHost'  => '127.0.0.1',
			'dbUser'  => 'survey',
			'dbPass'  => 'but there is always a horse',
			'dbName'  => 'survey',
			'default' => TRUE,
		),
	),

	/**
	 * Timezone Settings
	 */
	"system/timezone" => "Australia/Brisbane",
	"system/timeFormat" => "Y/m/d h:ia",

	/**
	 * Website URL
	 *   If not specified, it will attempt to resolve it on it's own.
	 *   If links are not working, please specify the URL here.
	 */
	//"system/urlroot" => "http://yourwebsitehere.com/",

	/**
	 * Website Paths
	 *   Specify the names of the folders where specific files are located.
	 *   This cannot be changed at runtime.
	 */
	"system/paths" => [
		'BASE'      => dirname(__FILE__),
		'CORE'      => "core/",
		'TEMPLATES' => "templates/",
		'PAGES'     => "pages/",
		'TASKS'     => "tasks/",
		'SURVEYS'   => "surveys/",
	],

	/**
	 * Default page
	 *   This the name of the default controller if one was not specified via. ?page=
	 *
	 *   This does not require the use of URL Rewrite as this has been removed from this
	 *   basic framework to work on a wider variety of servers with less configuration.
	 */
	"app/defaultpage" => 'Login',

	/**
	 * Session Management
	 *   This is the configuration for the session.
	 */
	"app/session" => [
		'cookieName'  => 'qutSurvey',
		'sessionTime' => 0, // Set to 0 for a session cookie or a time if you want an expiry.
		'sessionPath'   => '/',
	],

	/**
	 * Kakera Compability Settings
	 *   These are settings which are used in the KakeraSNS Framework on which
	 *   this simplified framework was based off.
	 */
	"system/ignorePackages" => [],
];
