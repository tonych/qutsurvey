-- phpMyAdmin SQL Dump
-- version 4.4.15
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2015 at 03:27 PM
-- Server version: 5.6.26-log
-- PHP Version: 5.6.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `survey`
--

-- --------------------------------------------------------

--
-- Table structure for table `flags`
--

CREATE TABLE IF NOT EXISTS `flags` (
  `flag_name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `flags`
--

INSERT INTO `flags` (`flag_name`, `description`) VALUES
('admin_user', 'This flag will allow the user access to the administration interface and survey information.');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_time` bigint(20) unsigned NOT NULL,
  `expiry_time` bigint(20) unsigned NOT NULL,
  `last_seen` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `user_id`, `start_time`, `expiry_time`, `last_seen`) VALUES
('03fb1742-5472-457e-99a8-5a74440c8118', 1, 1444105664, 1444107475, 1444105675),
('084269e8-e813-4d3b-82f1-65b081d8d8b8', 1, 1444105675, 1444107510, 1444105710),
('1561e209-ced5-451c-8002-169b4daa60e5', 1, 1444151053, 1444153819, 1444151119),
('1825b1ef-7d05-4caf-91bf-38bd9fa0bd99', 1, 1444110946, 1444112790, 1444110990),
('226f7a35-5267-4e1b-a9c5-ac43c16181d0', 1, 1444105710, 1444109155, 1444107355),
('33dfbb36-0765-47a5-9a95-6e8d56879640', 1, 1444136082, 1444138383, 1444136583),
('3cd46dcc-6ba5-49a9-9c6c-1095ceac43a4', 1, 1444136583, 1444139286, 1444137486),
('c740225c-d499-4d6b-a0ef-44eee568a9d1', 1, 1444146440, 1444149491, 1444146791);

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE IF NOT EXISTS `surveys` (
  `survey_id` int(8) unsigned NOT NULL,
  `survey_name` varchar(255) NOT NULL,
  `path` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `start_date` bigint(20) unsigned NOT NULL,
  `end_date` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`survey_id`, `survey_name`, `path`, `description`, `start_date`, `end_date`) VALUES
(1, 'Test Survey', 'oct2015', 'This is a test survey created to test functionality to the website.', 1442902630, 1443902630);

-- --------------------------------------------------------

--
-- Table structure for table `surveys_log`
--

CREATE TABLE IF NOT EXISTS `surveys_log` (
  `survey_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `event` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `timestamp` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surveys_log`
--

INSERT INTO `surveys_log` (`survey_id`, `user_id`, `event`, `data`, `timestamp`) VALUES
(1, 1, 'currStage', '"stage1-1-info"', 1444151055),
(1, 1, 'Introduction Complete.', '[]', 1444151055);

-- --------------------------------------------------------

--
-- Table structure for table `user_flags`
--

CREATE TABLE IF NOT EXISTS `user_flags` (
  `user_id` int(10) unsigned NOT NULL,
  `flag_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_surveys`
--

CREATE TABLE IF NOT EXISTS `user_surveys` (
  `survey_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `complete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_surveys`
--

INSERT INTO `user_surveys` (`survey_id`, `user_id`, `complete`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(8) unsigned NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `salt`) VALUES
(1, 'tonyc', 'f9eee0a66ceb554c1d9d454945ef3ae4c848802c0727b54ef571f145a4f9962b', '1f3b7745bc123e7614160d4735bc357bb6ec8813'),
(2, 'test', 'f9eee0a66ceb554c1d9d454945ef3ae4c848802c0727b54ef571f145a4f9962b', '1f3b7745bc123e7614160d4735bc357bb6ec8813');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `flags`
--
ALTER TABLE `flags`
  ADD PRIMARY KEY (`flag_name`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `surveys`
--
ALTER TABLE `surveys`
  ADD PRIMARY KEY (`survey_id`);

--
-- Indexes for table `surveys_log`
--
ALTER TABLE `surveys_log`
  ADD PRIMARY KEY (`event`,`survey_id`,`user_id`) USING BTREE;

--
-- Indexes for table `user_flags`
--
ALTER TABLE `user_flags`
  ADD PRIMARY KEY (`user_id`,`flag_name`);

--
-- Indexes for table `user_surveys`
--
ALTER TABLE `user_surveys`
  ADD PRIMARY KEY (`survey_id`,`user_id`),
  ADD KEY `users` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_flags`
--
ALTER TABLE `user_flags`
  ADD CONSTRAINT `users_k` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_surveys`
--
ALTER TABLE `user_surveys`
  ADD CONSTRAINT `user_surveys` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`survey_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
