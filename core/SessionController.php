<?php
class SessionController extends DatabaseController {
	protected $config;
	protected $sessionConfig;
	protected $session = NULL;
	protected $user = NULL;

	function __construct($tPath = NULL) {
		parent::__construct($tPath);

		global $config;
		$this->config = $config;

		// Handle Session Management.
		$this->sessionConfig = $config['app/session'];

		// Get any existing sessions.
		if (isset($_COOKIE[$this->sessionConfig['cookieName']])) {
			// Check cookie for session.
			$uuid = trim($_COOKIE[$this->sessionConfig['cookieName']]);

			$this->setSession($uuid);
		}
	}

	protected function requireLogin() {
		if (!$this->isLoggedIn()) {
			$this->redirect("Login", NULL);
		}
	}

	protected function requireAdmin() {
		if (!$this->isLoggedIn() || !$this->isAdmin($this->getCurrentUser()['user_id'])) {
			$this->redirect("Error", "notadmin");
		}
	}

	protected function isLoggedIn() {
		return $this->user !== NULL && $this->session !== NULL;
	}

	protected function getCurrentUser() {
		return $this->user;
	}

	protected function getSessionInfo() {
		return $this->session;
	}

	protected function createSession($user_id) {
		// Get user info
		$user = $this->getUserInfo($user_id);
		if (!$user) {
			return FALSE;
		}

		// Generate UUID
		do {
			// Generate a new UUID
			$uuid = get_uuid();

			// Check that the session does not exist.
			$rs = $this->dbc->select("sessions", ['session_id' => $uuid], ['session_id']);
		} while ($rs->fetchAssoc() !== FALSE);

		// Create in database
		$start_time = time();
		$last_seen = time();
		if ($this->config['app/session']['sessionTime'] == 0) {
			// This is a session cookie. Lets refresh by 30 minutes.
			$new_expiry = time() + 60 * 30;
			$cookie_expiry = 0;
		}
		else {
			// Update by the specified amount.
			$new_expiry = time() + $this->config['app/session']['sessionTime'];
			$cookie_expiry = $new_expiry;
		}

		// Insert into Database
		$this->dbc->insert("sessions", [
			"session_id"  => $uuid,
			"user_id"     => $user['user_id'],
			"start_time"  => $start_time,
			"expiry_time" => $new_expiry,
			"last_seen"   => $last_seen,
			]);

		// Set cookie
		setcookie($this->config['app/session']['cookieName'], $uuid, $cookie_expiry, $this->config['app/session']['sessionPath']);

		// Return session
		return $uuid;
	}

	protected function setSession($session_id) {
		// Check DB for existing session.
		$rs = $this->dbc->select("sessions", ["session_id" => $session_id]);
		$session = $rs->fetchAssoc();
		if ($session) {
			// We found a session. Lets see if it's still valid.
			if ($session['expiry_time'] <= time()) {
				// It's not valid. Lets delete their cookie.
				setcookie($this->config['app/session']['cookieName'], '', time()-3600, $this->config['app/session']['sessionPath']);
			}
			else {
				if ($this->config['app/session']['sessionTime'] == 0) {
					// This is a session cookie. Lets refresh by 30 minutes.
					$new_expiry = time() + 60 * 45;
					$cookie_expiry = 0;
				}
				else {
					// Update by the specified amount.
					$new_expiry = time() + $this->config['app/session']['sessionTime'];
					$cookie_expiry = $new_expiry;
				}
				$last_seen = time();

				// Update in DB
				$this->dbc->update("sessions",
					["expiry_time" => $new_expiry, "last_seen" => $last_seen],
					["session_id" => $session['session_id']]
				);

				// Update cookie
				setcookie($this->config['app/session']['cookieName'], $session['session_id'], $cookie_expiry, $this->config['app/session']['sessionPath']);

				// Update our local data
				$session['expiry_time'] = $new_expiry;
				$session['last_seen'] = $last_seen;

				// Log them in.
				$this->user = $this->getUserInfo($session['user_id']);
				$this->session = $session;
			}
		}
	}

	protected function getUserInfo($user_id) {
		$rs = $this->dbc->select('users', ['user_id' => $user_id]);

		return $rs->fetchAssoc();
	}

	protected function getUserId($username) {
		$rs = $this->dbc->select('users', ['username' => $username], ['user_id']);
		$row = $rs->fetchAssoc();
		if ($row) {
			return $row['user_id'];
		}

		return FALSE;
	}

	protected function getUserFlags($user_id) {
		$rs = $this->dbc->select('user_flags', ['user_id' => $user_id]);
		$flags = $rs->fetchAll();

		return $flags;
	}

	protected function isAdmin($user_id) {
		// Check Admin or not.
		$flags = $this->getUserFlags($user_id);
		return in_array("admin", $flags);
	}
}
