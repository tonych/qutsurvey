<?php
class DatabaseController extends Controller {
	protected $dbc;

	function __construct($tPath = NULL) {
		parent::__construct($tPath);

		global $config;

		// Load up configuration and Create a DB Connection.
		$this->dbc = kernel()->getDatabaseConnection();

		// Do... other things.
		unset($config['app/db']);
	}
}
