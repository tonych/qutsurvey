<?php
/**
 * PackageManager Class - Package Management and Loading.
 *
 * This is the package manager for Kakera. It loads up package metadata
 * automatically and loads packages when required.
 */
class PackageManager {
	protected $packagePath = array();
	protected $packages = array();
	protected $autoloadLookup = array();
	protected $kernel;

	public function __construct($kernel) {
		$this->kernel = $kernel;
		spl_autoload_register(array($this, 'autoload'));
	}

	/**
	 * Adds a path to the package manager.
	 *
	 * This adds the specified path to the package manager for loading.
	 *
	 * @param $path
	 * @throws KNotFoundException
	 */
	public function addPackagePath($path) {
		$this->packagePath[] = $path;
		$ignore = $this->kernel->config()->get('system/ignorePackages', array());
		//$ignore = array();

		if (is_dir($path)) {
			// Scan packages.
			$start_modules = array();
			$dir_listing = scandir($path);
			foreach ($dir_listing as $dir) {
				if (   is_dir($path . $dir)
					&& file_exists($path . $dir. '/package.info.php')
					&& !in_array($dir, $ignore)
				) {
					$info = include_once $path . $dir. '/package.info.php';

					if (   isset($info)
						&& isset($info['files'])
						&& isset($info['dependencies'])
						&& isset($info['name'])
						&& isset($info['autoload']))
					{
						foreach ($info['autoload'] as $class => $file) {
							$this->autoloadLookup[$class] = $dir;
						}
						$this->packages[$dir] = new Package($this->kernel, $info, $dir, $path . $dir . '/');

						if ((isset($info['load_immediately']) && $info['load_immediately'] === TRUE) ||
							(isset($info['loadImmediately']) && $info['loadImmediately'])) {
							$start_modules[] = $dir;
						}
					}
				}
			}

			// Attach Dependencies.
			foreach($this->packages as $p) {
				$dep = $p->getDependencies();
				foreach ($dep as $d) {
					if (isset($this->packages[$d])) {
						$p->attachDependency($this->packages[$d]);
					}
					else {
						throw new KNotFoundException(
							t(
								'Dependency @dependency was not found while resolving '
							   .'dependencies for @package.',
								array('@dependency' => $d, '@package' => $p)
							)
						);
					}
				}
			}

			// Load Immediate Packages
			foreach ($start_modules as $dir) {
				$this->packages[$dir]->load();
			}
		}
	}

	/**
	 * Gets all the paths containing packages to load.
	 */
	public function getPackagePaths() {
		return $this->packagePath;
	}

	/**
	 * Returns an array of all the packages found as Package Objects,
	 * allowing direct access to them.
	 */
	public function getPackages() {
		return $this->packages;
	}

	/**
	 * Gets a specific package by it's name.
	 * @param $name
	 */
	public function getPackage($name) {
		return isset($this->packages[$name]) ? $this->packages[$name] : NULL;
	}

	/**
	 * This autoloads packages if one of their autoload entries is called.
	 * @param $name
	 */
	public function autoload($name) {
		if (isset($this->autoloadLookup[$name])) {
			$this->loadPackage($this->autoloadLookup[$name]);
			$this->getPackage($this->autoloadLookup[$name])->loadAutoLoad($name);
		}
	}

	/**
	 * Gets an array of all the packages current loaded.
	 * @return array
	 */
	public function getLoadedPackages() {
		$loaded = array();
		foreach ($this->packages as $p) {
			if ($p->isLoaded()) {
				$loaded[] = $p->getShortName();
			}
		}

		return $loaded;
	}

	/**
	 * Gets an array containing the names of all the packages found.
	 */
	public function getAvailablePackages() {
		$avail = array();
		foreach ($this->packages as $p) {
			$avail[] = $p->getShortName();
		}

		return $avail;
	}

	/**
	 * Loads a package with the specified name.
	 *
	 * @param $name
	 */
	public function loadPackage($name) {
		return $this->packageExists($name) ? $this->getPackage($name)->load() : FALSE;
	}

	/**
	 * Checks if a package exists that has been found.
	 *
	 * @param $name
	 */
	public function packageExists($name) {
		return isset($this->packages[$name]);
	}
}
