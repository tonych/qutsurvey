<?php
/**
 * Kernel - KakeraSNS Kernel Shim
 *
 * This is a compatibility shim for the KakeraSNS Kernel, another project that
 * I have worked on previously, to allow packages and code to work without actually
 * running that framework.
 *
 * As this is a small project, the use of the entire KakeraSNS framework would be
 * excessive.
 *
 * @author Tony Chau
 */
class Kernel extends Extensible {
	const MIN_PHP_VER = '5.3.0';
	const VERSION     = '1.0';
	private $hooks = array();

	private $config;
	private $packages;

	/**
	 * Constructor
	 *
	 * This sets up the kernel and prepares it for processing. Before
	 * calling or initializing this method, the constant BASE_PATH and
	 * CORE_PATH must exist.
	 *
	 * If they do not exist, this will stop execution of the script,
	 * and print out a fatal error message.
	 *
	 * There are no external modules or OS dependancies.
	 */
	public function __construct() {
		global $config;

		// Load Configuration
		$this->config = $config;

		// Start Package Manager
		$this->packages = new PackageManager($this);
	}

	/**
	 * Accessor for the Log.
	 *
	 * @return Log
	 */
	public function log() {
		return [];
	}

	/**
	 * Accessor for the configuration.
	 *
	 * @return Config
	 */
	public function config() {
		return $this->config;
	}

	/**
	 * Accessor for the Packages.
	 *
	 * @return PackageManager
	 */
	public function packages() {
		return $this->packages;
	}

	/**
	 * Accessor for the WebRequest Object
	 *
	 * @return WebRequest
	 */
	public function request() {
		return NULL;
	}

	/**
	 * Accessor for the WebResponse Object
	 *
	 * @return WebResponse
	 */
	public function response() {
		return NULL;
	}

	/**
	 * Shutdowns the Server
	 *
	 * Shutdowns the server with the specified reason. Although in this
	 * version it is identical to die(), in future, a more advanced
	 * version of this method may be implemented.
	 *
	 * @param $reason
	 */
	public function shutdown($reason) {
		// Do nothing.
	}

	/**
	 * Checks that $path is contained within $base.
	 * @param $path
	 * @param $base
	 */
	public function checkPath($path, $base = NULL) {
		if ($base === NULL) {
			$base = BASE_PATH;
		}

		return (file_exists($path) && strpos(realpath($path), realpath($base)) === 0);
	}

	/**
	 * Adds a callback to be executed on $hook.
	 * @param $hook
	 * @param $callback
	 */
	public function addHook($hook, $callback) {
		if (is_callable($callback)) {
			if (isset($this->hooks[$hook])) {
				$this->hooks[$hook] = array($callback);
			}
			else {
				$this->hooks[$hook][] = $callback;
			}
		}
	}

	/**
	 * Calls a hook's callback functions.
	 * @param $hook
	 * @param $arguments
	 */
	public function callHook($hook, $arguments = array(), $stopWhenTrue = false) {
		$arguments['kernel'] = $this;
		if (isset($this->hooks[$hook])) {
			foreach ($this->hooks[$hook] as $c) {
				if (is_callable($c)) {
					$result = call_user_func($c, $arguments);
					if ($result !== NULL && $stopWhenTrue)
						return $result;
				}
			}
		}

		return NULL;
	}
}


/**
 * Translates, and processes strings.
 *
 * This function is a mix of sprintf() and filtering. It filters
 * for invalid characters, and performs filtering on strings.
 *
 * You should never pass variables as the text string.
 *
 * There are 2 types of placeholders which you can place.
 *
 * @var: Where var is the name of the content you want to put, it filters the
 *			 content before subsituting.
 * !var: Where var is the name of the content you want to put in, it places the
 *			 content in as is.
 */
function t($text, $array = array()) {
	foreach ($array as $k => $v) {
		switch($k[0]) {
			case '@':
				$v = Filter::filterText($v);
				break;
		}

		$text = str_replace($k, $v, $text);
	}

	return $text;
}

/**
 * Returns the kernel being used this instance.
 * @return Kernel
 */
function kernel() {
	if (isset($GLOBALS['kernel']))
		return $GLOBALS['kernel'];
	else
		return $GLOBALS['kernel'] = new Kernel();
}
