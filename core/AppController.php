<?php
class AppController extends SessionController {
	function __construct($tPath = NULL) {
		parent::__construct($tPath);

		$this->enableLayout = TRUE;
		$this->layout = "Default";
		$this->enableViewBag = TRUE;

		$this->viewBag['urlroot'] = getUrlRoot();
	}
}
