<?php
function f($text) {
	ini_set('mbstring.substitute_character', "none");
	$text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');
	return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
}

function checkPath($path, $base = NULL) {
	if ($base === NULL) {
		$base = BASE_PATH;
	}

	return (file_exists($path) && strpos(realpath($path), realpath($base)) === 0);
}
