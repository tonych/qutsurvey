<?php
class Controller {
	protected $controller;
	protected $action;
	protected $layout;
	protected $view;

	protected $styles = array();
	protected $title = "";
	protected $meta = array();
	protected $js = array();

	protected $enableLayout = TRUE;
	protected $viewBag = array();
	protected $enableViewBag = FALSE; // Default to False for compatibility.

	protected $templatePath;

	function __construct($tPath = NULL) {
		$this->controller = substr(get_class($this), 0, strrpos(get_class($this), "Controller"));
		if (is_null($tPath))
			$this->templatePath = TEMPLATES_PATH;
		else {
			$this->templatePath = $tPath;
		}
	}

	function loadAction($action) {
		$this->action = trim($action);
		if (empty($this->action)) {
			$this->action = 'index';
		}

		if (method_exists($this, $this->action)) {
			$callback = array($this, $this->action);
			$method = new ReflectionMethod($this, $this->action);
		}
		elseif (method_exists($this, 'defaultPage')) {
			$callback = array($this, 'defaultPage');
			$method = new ReflectionMethod($this, 'defaultPage');
		}

		if (isset($method) && $method->isPublic() && count($method->getParameters()) == 0) {
			ob_start();
			$return_code = call_user_func($callback);
			if ($this->enableViewBag) {
				$this->loadView("{$this->controller}/{$this->action}");
			}

			if ($this->enableLayout) {
				$content = ob_get_clean();
				$this->loadStyle($this->layout);

				$styles = "";
				foreach ($this->styles as $s) {
					$styles .= $s;
				}

				$meta = "";
				foreach ($this->meta as $name => $value) {
					$meta .= sprintf(
						'%s<meta name="%s" content="%s" />%s',
						"\t\t",
						f($name),
						f($value),
						"\n"
						);
				}

				$js = "";
				foreach ($this->js as $s) {
					$js .= sprintf(
						'%s<script type="text/javascript" src="%s"></script>%s',
						"\t\t",
						$s,
						"\n"
						);
				}

				$status = $this->loadView(
					$this->layout,
					array(
						'content' => $content,
						'styles' => $styles,
						'title' => $this->title,
						'meta' => $meta,
						'js' => $js,
						'urlroot' => getUrlRoot(),
						)
					);

				if (!$status) {
					throw new Exception(
						sprintf('Layout %s could not be found.',
							f($this->layout))
							);
				}
			}

			return $return_code;
		}
		else {
			throw new Exception(
				sprintf('%s access level does not allow calling via. URL in %s',
					f($this->action),
					f(get_class($this))
				));
		}

		throw new Exception(
			sprintf('%s could not be found in %s',
				f($this->action),
				f(get_class($this))
			));

	}

	protected function loadView($view, $data = array(), $return = FALSE) {
		// Check View Path.
		$view_path = $this->templatePath . $view . '.php';
		if (!file_exists($view_path)) {
			throw new Exception(sprintf(
				'%s view was not found.',
				f($view)
				));
		}

		// Load Styles
		$this->loadStyle($view);

		// Save State.
		$this->view = $view_path;
		$this->returnView = $return;

		// Clear (in case they get replaced by the variables below).
		unset($return);
		unset($view);
		unset($view_path);

		// Set Variables
		//foreach ($data as $k => $v) {
		//	$$k = $v;
		//}

		// Clear array.
		//unset($data);
		//unset($k);
		//unset($v);
		extract($data);
		if (count($data) == 0)
			extract($this->viewBag);
		unset($data);

		// Check if we are returning.
		ob_start();
		include $this->view;

		$content = ob_get_clean();

		if ($this->returnView) {
			return $content;
		}

		echo $content;
		return TRUE;
	}

	protected function loadStyle($style) {
		// Load CSS Styles if they exist.
		$css_path = $this->templatePath . $style . '.css';
		if (file_exists($css_path)) {
			$this->styles[$style] = str_replace('/*URLROOT*/', getUrlRoot(), file_get_contents($css_path));
		}
	}

	/**
	 * Adds Meta Tag
	 *
	 * Adds a Meta Tag to the HTML. This only allows name="" type meta tags.
	 * For http-equiv tags, please send HTTP headers instead via header().
	 *
	 * @param $name
	 *   This is the name of the meta tag you wish to add.
	 * @param $content
	 *   This contains the content of the meta tag.
	 */
	protected function setMeta($name, $content) {
		$this->meta[$name] = $content;
	}

	/**
	 * Loads an external JS file.
	 *
	 * This loads a javascript file into the website. This must be a URL.
	 *
	 * @param $src
	 *   This is the URL of the javascript file you wish to add.
	 */
	protected function loadJS($src) {
		if (!in_array($src, $this->js)) {
			$this->js[] = $src;
		}
	}

	protected function setTitle($title) {
		$this->title = $title;
	}

	protected function getTitle() {
		return $this->title;
	}

	protected function redirect($controller, $action = NULL, $args = []) {
		$url = getUrlToAction($controller, $action, $args);
		header("Location: {$url}");
		ob_end_clean();
		die();
	}
}
