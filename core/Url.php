<?php
function getUrlRoot($append = "") {
	global $config;

	if (array_key_exists('system/urlroot', $config) && $config['system/urlroot'] !== NULL) {
		return $config['system/urlroot'] . $append;
	}

	// Get the Hostname
	$name = $_SERVER['SERVER_NAME'];
	$port = '';

	// Determine the port via. protocol
	if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
		$proto = 'https';
		if ($_SERVER['SERVER_PORT'] != 443) {
			$port = sprintf(':%d', $_SERVER['SERVER_PORT']);
		}
	}
	else {
		$proto = 'http';
		if ($_SERVER['SERVER_PORT'] != 80) {
			$port = sprintf(':%d', $_SERVER['SERVER_PORT']);
		}
	}

	// Find the Dir which is running under.
	$dir = str_replace(basename($_SERVER['SCRIPT_FILENAME']), "", $_SERVER['SCRIPT_NAME']);

	// Cache Result
	$config['system/urlroot'] = sprintf("%s://%s%s%s", $proto, $name, $port, $dir);

	// Return the result.
	return $config['system/urlroot'] . $append;
}

function getCurrentUrl() {
   // Find the Dir which is running under.
   $dir = str_replace(basename($_SERVER['SCRIPT_FILENAME']), "", $_SERVER['SCRIPT_NAME']);

   // Remove the Dir from the the web_urlroot and append the REQUEST_URI
   // which contains the Dir.
   return substr_replace(getUrlRoot(), "", strrpos(getUrlRoot(), $dir)) . $_SERVER['REQUEST_URI'];
}

function getUrlPath($include_query_string = TRUE) {
	$stem = str_replace(getUrlRoot(), "", getCurrentUrl());
	if ($stem == getCurrentUrl()) {
		$stem = str_replace(substr(getUrlRoot(), 0, strlen(getUrlRoot()) - 1), "", getCurrentUrl());
	}

	if (!$include_query_string) {
		// Remove the Query String
		if (strpos($stem, "?") !== FALSE) {
			$stem = substr($stem, 0, strpos($stem, "?"));
		}
	}

	return trim($stem);
}

function getUrlToAction($controller, $action = NULL, $args = []) {
	$args['page'] = trim($controller);
	if ($action !== NULL) {
		$args['action'] = trim($action);
	}

	$qs = "";

	foreach ($args as $k => $v) {
		$k = urlencode($k);
		$v = urlencode($v);
		$qs .= "{$k}={$v}&";
	}
	$qs = substr($qs, 0, strlen($qs) - 1);

	return getUrlRoot("?{$qs}");	
}
