<?php
class SurveyController extends AppController {
	protected function isSignedUp() {
		// Check if the user has selected a survey
		if (!isset($_GET['survey'])) {
			return FALSE;
		}

		// Check if user is signed up to the survey.
		$rs = $this->dbc->select('user_surveys', [
			'user_id' => $this->getCurrentUser()['user_id'],
			'survey_id' => $_GET['survey'],
			]
		);

		if (!$rs->fetchAssoc()) {
			// not signed up for this survey.
			return FALSE;
		}

		return TRUE;
	}

	public function saveEvent($event, $data = [], $timestamp = NULL) {
		// Check if timestamp is NULL
		if (is_null($timestamp)) {
			$timestamp = time();
		}

		if ($this->loadEvent($event) !== NULL) {
			// save event.
			$this->dbc->update("surveys_log", [
				"data"      => json_encode($data),
				"timestamp" => $timestamp,
			],[
				"survey_id" => $_GET['survey'],
				"user_id"   => $this->getCurrentUser()['user_id'],
				"event"     => $event,
			]);
		}
		else {
			// save event.
			$this->dbc->insert("surveys_log", [
				"survey_id" => $_GET['survey'],
				"user_id"   => $this->getCurrentUser()['user_id'],
				"event"     => $event,
				"data"      => json_encode($data),
				"timestamp" => $timestamp,
				]);
		}
	}

	public function loadEvent($event) {
		// load event.
		$rs = $this->dbc->select("surveys_log", [
			"survey_id" => $_GET['survey'],
			"user_id"   => $this->getCurrentUser()['user_id'],
			"event"     => $event],
			['event', 'data', 'timestamp']
		);
		$event = $rs->fetchAssoc();
		if ($event) {
			$event['data'] = json_decode($event['data']);
		}
		else {
			$event = NULL;
		}
		return $event;
	}

	protected function loadSurvey($stage = NULL) {
		// Check for valid user session
		if (!$this->isLoggedIn()) {
			$this->redirect("Errors", "noSession");
		}

		// Check if they are signed up for this survey.
		if (!$this->isSignedUp()) {
			$this->redirect("Errors", "surveyUnavailable");
		}

		// Get survey data
		$rs = $this->dbc->select("surveys", ['survey_id' => $_GET['survey']]);

		// Check if they have completed this survey already, if so redirect.
		$urs = $this->dbc->select('user_surveys', ['survey_id' => $_GET['survey'], 'user_id' => $this->getCurrentUser()['user_id']]);
		$userSurvey = $urs->fetchAssoc();
		if ($userSurvey['complete'] > 0) {
			$this->redirect("Errors", "surveyComplete");
		}

		// Load Data
		$survey = $rs->fetchAssoc();

		// Check if the survey has expired or not.
		if ($survey['end_date'] < time()) {
			$this->redirect("Errors", "surveyUnavailable");
		}

		$relPath = "{$survey['path']}/";
		unset($rs);
		$this->setTitle($survey['survey_name']);

		// Include the survey info.php file.
		$surveyInfo = include SURVEY_PATH . $relPath . "info.php";

		// Check that the stage is included in the info package.
		if (!array_key_exists('stage', $_GET) || is_null($_GET['stage'])) {
			// Load current stage.
			$stage = $this->loadEvent("currStage");

			if (is_null($stage)) {
				// Create their current stage
				$stage = [$surveyInfo['stages'][0][0], $surveyInfo['defaultPage']];
				$this->saveEvent("currStage", $stage);
				$this->saveEvent("Started Survey");
			}
			else {
				$stage = $stage['data'];
				$foundStage = FALSE;
				foreach ($surveyInfo['stages'] as $s) {
					if ($s[0] == $stage[0]) {
						$foundStage = TRUE;
					}
				}
				if (!$foundStage) {
					$this->redirect("Errors", "stageMissing", ['req' => $stage]);
				}
			}
		}
		else {
			// Check that the stage exists...
			$stage = $_GET['stage'];
			if (!is_null($_GET['screen']))
				$screen = $_GET['screen'];
			else
				$screen = $surveyInfo['defaultPage'];

			foreach ($surveyInfo['stages'] as $v) {
				if ($stage == $v[0]) {
					$stage = [$v[0], $screen];
				}
			}

			if (!is_array($stage)) {
				$this->redirect("Errors", "stageMissing", ['req' => $stage]);
			}
		}

		// Load controller.
		ob_start();
		include_once SURVEY_PATH . $relPath . $stage[0] . '.php';
		$controllerName = $stage[0] . 'Controller';
		$controller = new $controllerName($this, $survey, $stage[0], $surveyInfo['viewPath']);
		$controller->loadAction($stage[1]);
		$this->styles = array_merge($this->styles, $controller->getStyles());
		$this->viewBag['content'] = ob_get_clean();
	}

	public function resume() {
		$this->loadSurvey();
	}
}

class StageController extends AppController {
	protected $survey;
	protected $info;
	protected $stage;
	protected $surveyController;

	public function __construct($sController, $survey, $stage, $tPath = NULL) {
		parent::__construct($tPath);

		$this->surveyController = $sController;
		$this->survey = $survey;
		$this->stage = $stage;

		$this->enableLayout = FALSE;	// There are no layouts with stages.


		// The path to the survey.
		$relPath = "{$this->survey['path']}/";

		// Include the survey info.php file.
		$this->info = include SURVEY_PATH . $relPath . "info.php";
	}

	public function start() {
		// The path to the survey.
		$relPath = "{$this->survey['path']}/";

		// Include the survey info.php file.
		$surveyInfo = $this->info;

		// Advance to next stage.
		for ($i = 0; $i < count($surveyInfo['stages']); $i++) {
			if ($surveyInfo['stages'][$i][0] == $this->stage && $this->surveyController->loadEvent("{$surveyInfo['stages'][$i][1]} Started.") === NULL) {
				$this->surveyController->saveEvent("{$surveyInfo['stages'][$i][1]} Started.");
				break;
			}
		}
	}

	protected function getUrlToStage($stage, $screen) {
		return getUrlToAction('Survey', 'resume', ['survey' => $this->survey['survey_id'], 'stage' => $stage, 'screen' => $screen]);
	}

	protected function redirectToStage($stage, $screen) {
		return $this->redirect('Survey', 'resume', ['survey' => $this->survey['survey_id'], 'stage' => $stage, 'screen' => $screen]);
	}

	public function nextStage() {
		// Disable view bag since we don't have any thing.
		$this->enableViewBag = FALSE;

		// The path to the survey.
		$relPath = "{$this->survey['path']}/";

		// Include the survey info.php file.
		$surveyInfo = $this->info;

		// Advance to next stage.
		for ($i = 0; $i < count($surveyInfo['stages']); $i++) {
			if ($surveyInfo['stages'][$i][0] == $this->stage) {
				$this->surveyController->saveEvent("{$surveyInfo['stages'][$i][1]} Complete.");
				$stage = $surveyInfo['stages'][$i + 1];

				$screen = $surveyInfo['defaultPage'];
				$stage = [$stage[0], $screen];
				$this->surveyController->saveEvent("currStage", $stage);
				$this->surveyController->redirect("Survey", "resume", ['survey' => $this->survey['survey_id']]);
				break;
			}
		}
	}

	protected function isStageComplete() {
		// The path to the survey.
		$relPath = "{$this->survey['path']}/";

		// Include the survey info.php file.
		$surveyInfo = include SURVEY_PATH . $relPath . "info.php";

		// Search for the stage name.
		for ($i = 0; $i < count($surveyInfo['stages']); $i++) {
			if ($surveyInfo['stages'][$i][0] == $this->stage && $this->surveyController->loadEvent("{$surveyInfo['stages'][$i][1]} Complete.") !== NULL) {
				return TRUE;
			}
		}
		return FALSE;
	}

	public function getStyles() {
		return $this->styles;
	}
}
