<?php
class AdminController extends AppController {
  public function isAdmin() {
    $user = $this->getCurrentUser();
    $flags = $this->dbc->select('user_flags',[
      "user_id" => $user['user_id']
      ],
      ['flag_name']
    );

    foreach ($flags as $f) {
      if ($f == "admin_user")
        return TRUE;
    }

    return FALSE;
  }

  
}