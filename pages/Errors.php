<?php
class ErrorsController extends AppController {
	function __construct() {
		parent::__construct();
	}

	function surveyComplete() {
		$this->setTitle("Survey Complete");
	}

	function noSession() {
		$this->setTitle("Login Required");
	}

	function surveyUnavailable() {
		$this->setTitle("Survey no longer available");
	}

	function stageMissing() {
		$this->setTitle("Survey Error");

		$this->dbc->insert("errors", [
			'user_id'   => ($this->getCurrentUser() !== NULL) ? $this->getCurrentUser()['user_id'] : NULL,
			'timestamp' => time(),
			'message'   => json_encode(
				['g' => $_GET, 'p' => $_POST, 'c' => $this]
				),
			]);
	}
}
