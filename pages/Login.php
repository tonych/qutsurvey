<?php
class LoginController extends AppController {
	function __construct() {
		parent::__construct();
	}

	function index() {
		$this->setTitle("QUT Surveys");
		if ($this->isLoggedIn()) {
			$this->redirect("Home");
		}
		if (isset($_GET['error'])) {
			$this->viewBag['error'] = $_GET['error'];
		}
	}

	function check() {
		$this->enableLayout = FALSE;
		$this->enableViewBag = FALSE;

		if (!isset($_POST['username']) || !isset($_POST['password'])) {
			$this->redirect('Login', 'index', ['error' => 'noinput']);
		}

		// Get the entered username and password.
		$un = $_POST['username'];
		$pw = $_POST['password'];

		// Check for username in system.
		$uid = $this->getUserId($un);
		if ($uid !== FALSE) {
			// Get User information
			$user = $this->getUserInfo($uid);

			// Check password matches.
			$hashedpw = hash("sha256", $pw . $user['salt'], FALSE);
			if ($hashedpw == $user['password']) {
				// It matches, lets create a session.
				$session = $this->createSession($uid);
				$this->setSession($session);
				$this->redirect('Home');
			}
		}

		// Redirect with error
		$this->redirect('Login', 'index', ['error' => 'pwuser']);
	}

	function logout() {
		$this->requireLogin();

		// Get their current session
		$session = $this->getSessionInfo();
		$expiry_time = time() - 60;

		// Update the session time.
		$this->dbc->update("sessions",
			['expiry_time' => $expiry_time],
			['session_id' => $session['session_id']]
		);

		// Clear the current user/session data from the controller.
		$this->session = NULL;
		$this->user = NULL;

		// Run the default code to refresh their session.
		$this->setSession($session['session_id']);

		// Redirect to login page.
		$this->requireLogin();
	}
}
