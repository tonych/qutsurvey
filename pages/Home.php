<?php
class HomeController extends AppController {
	function __construct() {
		parent::__construct();
		$this->requireLogin();
	}

	function index() {
		// If admin, redirect to admin home.
		if ($this->isAdmin($this->getCurrentUser()['user_id'])) {
			$this->redirect('Home', 'adminHome');
		}

		// Load up all their surveys.
		$rs = $this->dbc->select('user_surveys',
			['user_id' => $this->getCurrentUser()['user_id']],
			['survey_id', 'complete']
		);
		$survey_ids = $rs->fetchAll();
		$surveys = [];
		$i = -1;
		foreach ($survey_ids as $s) {
			$rs = $this->dbc->select('surveys', ['survey_id' => $s['survey_id']]);
			$surveys[++$i] = $rs->fetchAssoc();
			$surveys[$i]['complete'] = $s['complete'];
		}

		// If they only have one survey, redirect them to it.
		if (count($surveys) == 1) {
			$this->redirect('Survey', 'resume', ['survey' => $surveys[0]['survey_id']]);
		}

		// Else build a list of their surveys and statuses.
		$this->viewBag['surveys'] = $surveys;
	}

	function adminHome() {
		$this->enableViewBag = FALSE;
		echo "Admin interface to be implemented. ";
	}
}
